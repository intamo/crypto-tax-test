const coinStorage = [];

const BuyCoins = (type = "", coin = 0, price = 0) => {
  let coinModel = {
    type: type,
    coin: coin,
    price: price,
  };
  coinStorage.push(coinModel);
  console.log("Buy coins : ", coinModel);
};

const SaleCoins = (type = "", coinNumber = 0, currentPrice = 0) => {
  let currentCoin = coinNumber;
  let sum = 0;
  let profit = 0;

  coinStorage.forEach((element) => {
    if (type == element.type) {
      sum += element.coin;
    }
  });

  if (sum == 0 || sum < currentCoin) {
    console.log("Not enough Coin");
  } else {
    coinStorage.forEach((item) => {
      if (type == item.type) {
        // coin < in storage
        if (currentCoin < item.coin) {
          profit += (item.price - currentPrice) * currentCoin;
          item.coin -= currentCoin;
          currentCoin = 0;
        }

        // coin >= in storage
        if (currentCoin >= item.coin) {
          profit += (item.price - currentPrice) * item.coin;
          currentCoin -= item.coin;
          item.coin = 0;
        }
      }
    });

    coinStorage.forEach((item, index, object) => {
      if (item.coin == 0) {
        object.splice(index, 1);
      }
    });

    console.log(
      `Sale ${type} ${coinNumber} at ${currentPrice} total price = ${profit}`
    );
    console.log("Coin storage : ", coinStorage);
  }
};

BuyCoins("BTC", 2.5, 680000.0);
BuyCoins("ETH", 12.0, 43000.0);
BuyCoins("BTC", 2.5, 690000);
SaleCoins("BTC", 3.0, 695000.0);
BuyCoins("ETH", 13.5, 43500.0);
SaleCoins("BTC", 1, 695000.0);
SaleCoins("ETH", 30.0, 45000.0);
